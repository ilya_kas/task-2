package by.epamtc.jwd.task.exception;

public class WrongMassException extends Exception{
    public WrongMassException() {
        super();
    }

    public WrongMassException(String message) {
        super(message);
    }

    public WrongMassException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongMassException(Throwable cause) {
        super(cause);
    }
}
