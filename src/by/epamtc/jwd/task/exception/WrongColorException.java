package by.epamtc.jwd.task.exception;

public class WrongColorException extends Exception{
    public WrongColorException() {
        super();
    }

    public WrongColorException(String message) {
        super(message);
    }

    public WrongColorException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongColorException(Throwable cause) {
        super(cause);
    }
}
