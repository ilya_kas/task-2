package by.epamtc.jwd.task.util;

import java.util.Scanner;

public class Communicator {

    Scanner scan = new Scanner(System.in);

    public int nextInt(){
        while (!scan.hasNextInt()) {
            scan.next();
            System.out.println("Input an integer");
        }
        int num = scan.nextInt();
        return num;
    }
}
