package by.epamtc.jwd.task.entity;

import by.epamtc.jwd.task.exception.WrongColorException;
import by.epamtc.jwd.task.exception.WrongMassException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Basket {
    private final double MIN_BALL_MASS = 0.1;

    private List<Ball> balls = new ArrayList<>();
    private Random random = new Random();

    public void add(Ball ball){
        if (ball!=null)
            balls.add(ball);
    }

    public double getSumMass(){
        double mass = 0;
        for (Ball ball: balls)
            mass += ball.getMass();

        return mass;
    }

    public int countBlueBalls(){
        int count = 0;
        for (Ball ball: balls)
            if (ball.getColor() == Color.BLUE)
                count++;

        return count;
    }

    /**
     * Заполенение корзины не менее 3 мячами
     * @param n желаемое количество мячей в корзине
     */
    public void fillRandom(int n){
        balls.clear();

        try {
            add(new Ball(random.nextDouble(), Color.BLACK)); //предпрошитое заполнение корзины мячиками
            add(new Ball(random.nextDouble(), Color.BLUE));
            add(new Ball(random.nextDouble(), Color.RED));
            for (int i=3;i<n;i++)
                add(new Ball(Math.abs(random.nextDouble())*10 + MIN_BALL_MASS, Math.abs(random.nextInt()) % Color.values().length));
        }catch (WrongColorException | WrongMassException e){
            //log
            System.out.println("Wrong ball parameter");
        }
    }
}
