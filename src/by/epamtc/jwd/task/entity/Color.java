package by.epamtc.jwd.task.entity;

import by.epamtc.jwd.task.exception.WrongColorException;

public enum Color {
    BLUE, RED, BLACK, WHITE;

    public static Color get(int color) throws WrongColorException {
        if (color<0 || color>= values().length)
            throw new WrongColorException();

        Color[] val = Color.values();
        return val[color];
    }
}
