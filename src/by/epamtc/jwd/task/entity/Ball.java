package by.epamtc.jwd.task.entity;

import by.epamtc.jwd.task.exception.WrongColorException;
import by.epamtc.jwd.task.exception.WrongMassException;

public class Ball {

    private double mass;
    private Color color;

    public Ball(double mass, Color color) throws WrongMassException {
        if (mass <= 0)
            throw new WrongMassException();
        this.mass = mass;
        this.color = color;
    }

    public Ball(double mass, int color) throws WrongColorException, WrongMassException {
        if (mass <= 0)
            throw new WrongMassException();
        this.mass = mass;
        this.color = Color.get(color);
    }

    public double getMass() {
        return mass;
    }

    public Color getColor() {
        return color;
    }
}
