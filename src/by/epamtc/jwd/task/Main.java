package by.epamtc.jwd.task;

import by.epamtc.jwd.task.entity.Basket;
import by.epamtc.jwd.task.util.Communicator;

public class Main {
    public static void main(String[] args) {
        Communicator in = new Communicator();

        int n = in.nextInt();
        if (n<3) {
            System.out.println("Enter a larger number");
            return;
        }

        Basket basket = new Basket();  //создание корзины
        basket.fillRandom(n);

        System.out.println(basket.getSumMass());
        System.out.println(basket.countBlueBalls());
    }
}
